/*global window*/
/*jslint maxlen: 100, plusplus: true, unparam: true */
(function () {
  "use strict";

  var lastTime = 0,
    vendors = ['ms', 'moz', 'webkit', 'o'],
    x;

  for (x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame']
      || window[vendors[x] + 'CancelRequestAnimationFrame'];
  }

  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function (callback, element) {
      var currTime = new Date().getTime(),
        timeToCall = Math.max(0, 16 - (currTime - lastTime)),
        id = window.setTimeout(function () {
          callback(currTime + timeToCall);
        }, timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };
  }

  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function (id) {
      window.clearTimeout(id);
    };
  }
}());

(function () {
  'use strict';
  var leftBtn, rightBtn, carousel, carouselItems, carouselContainer,
    visibleWidth, edgeWidth, carouselWidth, maxScrollTo, numCards, cardWidth, cardMargin,
    animationId, scrolling;

  numCards = 12;
  cardWidth = 100;
  cardMargin = 5;
  edgeWidth = 200;
  scrolling = false;

  var startValue, changeInValue;
  var newPosition;


  function easeInCubic(currentIteration, startValue, changeInValue, totalIterations) {
    return changeInValue * Math.pow(currentIteration / totalIterations, 3) + startValue;
  }

  function easeOutCubic(currentIteration, startValue, changeInValue, totalIterations) {
    return changeInValue * (Math.pow(currentIteration / totalIterations - 1, 3) + 1) + startValue;
  }

  function discoverElements() {
    carousel = document.getElementById('carousel');
    leftBtn = document.getElementById('btn-left');
    rightBtn = document.getElementById('btn-right');
    carouselContainer = document.getElementById('carouselContainer');

    carouselItems = carousel.children;
  }

  function bindKeys() {

    /**
     * left button
     */
    leftBtn.addEventListener('click', function () {

      // debounce
      if (scrolling) {
        return;
      }

      // if starting position is at min, then don't scroll
      if (carousel.scrollLeft == 0) {
        return;
      }

      // calculate final position
      var to = carousel.scrollLeft - visibleWidth;
      to = to < 0 ? 0 : to;

      startValue = carousel.scrollLeft;
      changeInValue = to - startValue;

      scrolling = true;

      var totalIterations = 50;
      var iterationCount = 0;

      scrollAnimation(function (interval) {
          newPosition = easeOutCubic(iterationCount, startValue, changeInValue, totalIterations);
          iterationCount++;

          //var pxShift = 10 * interval / 16;// 60fps ~= 16ms
          //var newPosition = carousel.scrollLeft - pxShift;
          // ensure that px shift does not go below min
          //newPosition = newPosition < 0 ? 0 : newPosition;

          carousel.scrollLeft = newPosition;
          document.getElementById('scrollLeft').innerHTML = carousel.scrollLeft;

          if (carousel.scrollLeft <= to) {
            scrolling = false;
            return false;
          }
        },
        carousel,
        function () {
        });
    });

    /**
     * right button
     */
    rightBtn.addEventListener('click', function () {

      // debounce
      if (scrolling) {
        return;
      }

      // if starting position is at max, then don't scroll
      if (carousel.scrollLeft >= maxScrollTo) {
        return;
      }

      // calculate final position
      var to = carousel.scrollLeft + visibleWidth;
      to = to > maxScrollTo ? maxScrollTo : to;

      startValue = carousel.scrollLeft;
      changeInValue = to - startValue;

      scrolling = true;

      var totalIterations = 50;
      var iterationCount = 0;

      scrollAnimation(function (interval) {

          newPosition = easeOutCubic(iterationCount, startValue, changeInValue, totalIterations);
          iterationCount++;

          //var pxShift = 10 * interval / 16; // 10px per sec
          //var newPosition = carousel.scrollLeft + pxShift;
          // ensure that px shift does not go beyond max
          //newPosition = newPosition > to ? to : newPosition;

          carousel.scrollLeft = newPosition;
          document.getElementById('scrollLeft').innerHTML = carousel.scrollLeft;

          if (carousel.scrollLeft >= to) {
            scrolling = false;
            return false;
          }
        },
        carousel,
        function () {
        });
    });

    /**
     * carousel
     */
    carousel.addEventListener('click', function () {
      if (scrolling) {
        cancelAnimationFrame(animationId);
        scrolling = false;
      }
    });
  }

  function scrollAnimation(render, element, onComplete) {
    var running,
      lastFrame = new Date().getTime();

    function loop(now) {
      if (running !== false) {
        animationId = requestAnimationFrame(loop, element);
        var interval = now - lastFrame;
        // if browser tab is inactive, requestAnimationFrame may be throttled by the browser vendor
        if (interval < 160 && interval > 0) {
          running = render(interval);
        }
        lastFrame = now;
      }
      else {
        cancelAnimationFrame(animationId);

        if (typeof(onComplete) !== 'undefined') {
          onComplete();
        }
      }
    }

    loop(lastFrame);
  }

  function calcDimensions() {
    // determine carousel width. This is the card width * no. cards. Can't use clientWidth as this
    // changes with viewport
    carouselWidth = ((cardWidth + cardMargin) * numCards) - cardMargin; // no margin on last card
    document.getElementById('carouselWidth').innerHTML = carouselWidth;

    // determine width of carousel container allowing for edge overlays
    visibleWidth = carouselContainer.offsetWidth - (edgeWidth * 2);
    document.getElementById('visibleWidth').innerHTML = visibleWidth;

    // calculate max scrollLeft. This is carousel width - visible width
    maxScrollTo = carouselWidth - visibleWidth;
    document.getElementById('maxScrollTo').innerHTML = maxScrollTo;

    document.getElementById('scrollLeft').innerHTML = carousel.scrollLeft;
  }

  function init() {
    discoverElements();
    calcDimensions();
    bindKeys();

    window.addEventListener('resize', handleResize, false);
  }

  function handleResize() {
    calcDimensions();
  }

  // IE 9+ see: http://beeker.io/jquery-document-ready-equivalent-vanilla-javascript
  var domReady = function (callback) {
    document.readyState === "interactive" || document.readyState === "complete" ? callback() : document.addEventListener("DOMContentLoaded", callback);
  };

  domReady(function () {
    init();
  });
})();